package ru.t1consulting.vmironova.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.enumerated.CustomSort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private CustomSort sort;

    public TaskListRequest(@Nullable String token) {
        super(token);
    }

}
