package ru.t1consulting.vmironova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.vmironova.tm.api.endpoint.UserEndpoint;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.t1consulting.vmironova.tm.api.endpoint.UserEndpoint")
public class UserRestEndpointImpl implements UserEndpoint {

    @Autowired
    private IUserDTOService userService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<UserDTO> findAll() throws Exception {
        return userService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    public UserDTO add(
            @WebParam(name = "user", partName = "user")
            @RequestBody final @NotNull UserDTO user
    ) throws Exception {
        return userService.add(user);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public UserDTO save(
            @WebParam(name = "user", partName = "user")
            @RequestBody final @NotNull UserDTO user
    ) throws Exception {
        return userService.update(user);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public UserDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        return userService.findOneById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        return (userService.findOneById(id) != null);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return userService.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final @NotNull String id
    ) throws Exception {
        userService.removeById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody final @NotNull UserDTO user
    ) throws Exception {
        userService.remove(user);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        userService.clear();
    }


}
